package com.game.player

import akka.actor.{ActorSystem, Props}
import com.game.player.PlayerActions.StartGame

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Created by nk91 on 13.04.17.
  */
object PinPongGame extends App {

  val system       = ActorSystem("ping-pong-game")
  val firstPlayer  = system.actorOf(createPlayer("Player-1"))
  val secondPlayer = system.actorOf(createPlayer("Player-2"))

  def createPlayer(name: String): Props =
    Player.props(name, pingsPerPong = 2, pongsPerSet = 3, sets = 3, pingDelay = 1 second, exceptionDelay = 2 seconds)

  firstPlayer ! StartGame(secondPlayer)
  Await.result(system.whenTerminated, Duration.Inf)

}
