package com.game.player

import akka.actor.{ActorRef, Cancellable}
import akka.persistence.fsm.PersistentFSM
import akka.persistence.fsm.PersistentFSM.FSMState
import com.game.player.PlayerActions.PingTimeout
import com.game.player.PlayerDomainEvents.PlayerDomainEvent
import com.game.player.PlayerStateData.StateData
import com.game.player.PlayerStates.{PingState, PlayerState, PongState, RoleChangeState}
import com.game.player.Simulations.Oops

import scala.concurrent.duration.FiniteDuration
import scala.reflect.{ClassTag, classTag}

/**
  * Created by kisilnazar on 15.04.17.
  */
abstract class PlayerActor(name: String) extends PersistentFSM[PlayerState, StateData, PlayerDomainEvent] {

  override def domainEventClassTag: ClassTag[PlayerDomainEvent] = classTag[PlayerDomainEvent]

  override def persistenceId: String = name

  override def journalPluginId = "akka.persistence.journal.inmem"

  override def snapshotPluginId = "akka.persistence.snapshot-store.local"

  def setPingDelay(delay: FiniteDuration): Cancellable =
    context.system.scheduler.scheduleOnce(delay, self, PingTimeout)(context.dispatcher)

  def setErrorDelay(delay: FiniteDuration): Cancellable =
    context.system.scheduler.schedule(delay, delay, self, Oops)(context.dispatcher)

  def info(message: => String) = {
    val logColor = stateName match {
      case RoleChangeState => Console.WHITE
      case PongState       => Console.RED
      case PingState       => Console.GREEN
      case _               => Console.WHITE
    }
    log.info(s"$logColor[$name][$stateName]" + message + Console.WHITE)
  }

}

object PlayerStateData {

  sealed trait StateData

  case class PingStateData(receivedPongs: Int, currentSet: Int, pingTimeout: Cancellable, target: ActorRef)
      extends StateData

  case class PongStateData(receivedPings: Int, exceptionCancellable: Cancellable, target: ActorRef) extends StateData

  case object NoData extends StateData

}

object PlayerStates {

  sealed trait PlayerState extends FSMState

  case object PingState extends PlayerState {
    override def identifier: String = "PingState"
  }

  case object PongState extends PlayerState {
    override def identifier: String = "PongState"
  }

  case object Pending extends PlayerState {
    override def identifier: String = "Pending"
  }

  case object RoleChangeState extends PlayerState {
    override def identifier: String = "RoleChangeState"
  }

}

object PlayerDomainEvents {

  sealed trait PlayerDomainEvent

  case class StartPing(opponent: ActorRef, pingTimeout: Cancellable) extends PlayerDomainEvent

  case class UpdatePingState(receivedPongs: Int, currentSet: Int, pingTimeout: Cancellable, target: ActorRef)
      extends PlayerDomainEvent

  case class UpdatePongState(receivedPings: Int, exceptionTimeout: Cancellable, target: ActorRef)
      extends PlayerDomainEvent

}

object PlayerActions {

  sealed trait Action

  case class StartGame(opponent: ActorRef) extends Action

  case class RoleChange(currentSet: Int) extends Action

  case object Ping extends Action

  case object Pong extends Action

  case object ConfirmRoleChange extends Action

  case object PingTimeout extends Action

  case object FinishGame extends Action

}

object Simulations {

  sealed trait Simulation

  case object Oops extends Simulation

  case object CurrentState extends Simulation

}
