package com.game.player

import akka.actor.{PoisonPill, Props}
import com.game.player.PlayerActions._
import com.game.player.PlayerDomainEvents._
import com.game.player.PlayerStateData.{NoData, PingStateData, PongStateData, StateData}
import com.game.player.PlayerStates._
import com.game.player.Simulations.{CurrentState, Oops}

import scala.concurrent.duration._

/**
  * Created by kisilnazar on 11.04.17.
  */
class Player(name: String,
             pingDelay: FiniteDuration,
             pingsPerPong: Int,
             pongsPerSet: Int,
             gamesSets: Int,
             errorDelay: FiniteDuration)
    extends PlayerActor(name) {

  startWith(Pending, NoData)

  when(Pending) {

    case Event(StartGame(opponent), _) =>
      info("[StartPing]")
      goto(PingState) applying StartPing(opponent, setPingDelay(pingDelay))

    case Event(Ping, _) =>
      info("[Ping]")
      goto(PongState) applying UpdatePongState(1, setErrorDelay(errorDelay), sender()) andThen {
        case state: PongStateData => if (canReplayForPing(state.receivedPings)) state.target ! Pong
      }

  }

  when(PingState) {

    case Event(PingTimeout, PingStateData(pongs, set, _, target)) =>
      info(s"[PingTimeout]")
      stay() applying UpdatePingState(pongs, set, setPingDelay(pingDelay), target) andThen { _ =>
        target ! PlayerActions.Ping
      }

    case Event(Pong, PingStateData(pongs, set, pingTimeout, target)) =>
      val receivedPongs = pongs + 1
      info(s"[Pong] $receivedPongs")
      if (canChangeRole(receivedPongs)) {
        val currentSets = set + 1
        if (canFinishGame(currentSets)) {
          target ! FinishGame
          self ! PoisonPill
          stay()
        } else {
          goto(RoleChangeState) applying UpdatePingState(receivedPongs, currentSets, pingTimeout, target) andThen { _ =>
            target ! RoleChange(currentSets)
          }
        }
      } else stay() applying UpdatePingState(receivedPongs, set, pingTimeout, target)

  }

  when(RoleChangeState) {

    case Event(PingTimeout, PingStateData(pongs, set, _, target)) =>
      stay() applying UpdatePingState(pongs, set, setPingDelay(pingDelay), target) andThen { _ =>
        target ! RoleChange(set)
      }

    case Event(ConfirmRoleChange, PingStateData(_, _, pingTimeout, target)) =>
      pingTimeout.cancel()
      goto(PongState) applying UpdatePongState(receivedPings = 0, setErrorDelay(errorDelay), target)

  }

  when(PongState) {

    case Event(Ping, PongStateData(pings, exceptionTimeout, target)) =>
      val receivedPings = pings + 1
      info(s"[Ping] $receivedPings ")
      stay() applying UpdatePongState(receivedPings, exceptionTimeout, target) andThen { _ =>
        if (canReplayForPing(receivedPings)) target ! Pong
      }

    case Event(RoleChange(currentSets), PongStateData(_, exceptionTimeout, _)) =>
      info(s"[RoleChange]")
      goto(PingState) applying UpdatePingState(receivedPongs = 0, currentSets, setPingDelay(pingDelay), sender()) andThen {
        _ =>
          exceptionTimeout.cancel()
          sender() ! ConfirmRoleChange
      }

    case Event(Oops, PongStateData(pings, _, _)) =>
      info(s"[Oops] pings=$pings")
      throw new RuntimeException("Oops! Something goes wrong!")

  }

  whenUnhandled {

    case Event(FinishGame, _) =>
      info(s"[FinishGame]")
      self ! PoisonPill
      context.system.terminate()
      stay()

    case Event(CurrentState, stateData) =>
      info(s"[CurrentState] stateData=$stateData")
      sender() ! (stateName, stateData)
      stay()

  }

  override def applyEvent(domainEvent: PlayerDomainEvent, data: StateData): StateData = domainEvent match {
    case StartPing(target, cancel)                        => PingStateData(receivedPongs = 0, currentSet = 1, cancel, target)
    case UpdatePingState(pongs, set, pingTimeout, target) => PingStateData(pongs, set, pingTimeout, target)
    case UpdatePongState(pings, exceptionTimeout, target) => PongStateData(pings, exceptionTimeout, target)
  }

  private def canChangeRole(receivedPongs: Int) = receivedPongs % pongsPerSet == 0

  private def canFinishGame(currentSets: Int) = currentSets >= gamesSets

  private def canReplayForPing(receivedPings: Int) = receivedPings % pingsPerPong == 0

}

object Player {

  def props(name: String,
            pingsPerPong: Int,
            pongsPerSet: Int,
            sets: Int,
            pingDelay: FiniteDuration,
            exceptionDelay: FiniteDuration): Props =
    Props(classOf[Player], name, pingDelay, pingsPerPong, pongsPerSet, sets, exceptionDelay)

}
