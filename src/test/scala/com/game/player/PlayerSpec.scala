package com.game.player

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import com.game.player.PlayerActions._
import com.game.player.PlayerStateData.{NoData, PingStateData, PongStateData}
import com.game.player.PlayerStates.{PlayerState, PongState}
import com.game.player.Simulations.{CurrentState, Oops}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration._

/**
  * Created by nk91 on 13.04.17.
  */
class PlayerSpec
    extends TestKit(ActorSystem("PlayerSpec"))
    with ImplicitSender
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  override def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  "Player actor" should {

    "stay in Pending state after creation" in {
      val player = createDefaultPlayerActor("test")
      player.underlyingActor.stateName === PlayerStates.Pending
      player.underlyingActor.stateData === NoData
      system.stop(player)
    }

    "go to Ping state after StartGame message" in {
      val player = createDefaultPlayerActor("test")
      player ! StartGame(self)
      player.underlyingActor.stateName === PlayerStates.PingState
      system.stop(player)
    }

    "sent Ping message in Ping state" in {
      val player = system.actorOf(defaultPlayerProps("test"))
      player ! StartGame(self)
      player ! PingTimeout
      expectMsg(Ping)
      system.stop(player)
    }

    "increment receivedPongs after receive Pong in PingState" in {
      val player = system.actorOf(defaultPlayerProps("test"))
      player ! StartGame(self)
      player ! Pong
      player ! CurrentState
      val (state: PlayerState, stateData: PingStateData) = expectMsgClass(classOf[(PlayerState, PingStateData)])
      stateData.receivedPongs === 1
      system.stop(player)
    }

    "increment currentSet after received pongsPerSet" in {
      val player = system.actorOf(
        Player.props("test", pingsPerPong = 4, pongsPerSet = 2, sets = 5, pingDelay = 1 hour, exceptionDelay = 1 hour))
      player ! Pong
      player ! Pong
      val roleChange = expectMsgClass(classOf[RoleChange])
      roleChange.currentSet === 2
      player ! CurrentState
      val (state: PlayerState, stateData: PingStateData) = expectMsgClass(classOf[(PlayerState, PingStateData)])
      state === PlayerStates.RoleChangeState
      stateData.currentSet === 2
      system.stop(player)
    }

    "repeat RoleChangeMessage message until receiving confirmation" in {
      val player = system.actorOf(
        Player.props("test-confirmation",
                     pingsPerPong = 1,
                     pongsPerSet = 1,
                     sets = 5,
                     pingDelay = 100 millis,
                     exceptionDelay = 1 hour))
      player ! StartGame(self)
      expectMsg(Ping)
      player ! Pong
      expectMsgClass(classOf[RoleChange])
      expectMsgClass(classOf[RoleChange])
      player ! ConfirmRoleChange
      player ! Ping
      expectMsg(Pong)
      player ! Ping
      expectMsg(Pong)
      player ! CurrentState
      val (state: PlayerState, stateData: PongStateData) = expectMsgClass(classOf[(PlayerState, PongStateData)])
      state === PlayerStates.RoleChangeState
      stateData.receivedPings === 2
      system.stop(player)
    }

    "from Pending to PongState after Ping receiving" in {
      val player = system.actorOf(
        Player.props("test-pong-state",
                     pingsPerPong = 1,
                     pongsPerSet = 2,
                     sets = 2,
                     pingDelay = 1 hour,
                     exceptionDelay = 1 hour)
      )
      player ! Ping
      expectMsg(Pong)
      system.stop(player)
    }

    "recover received pings after actor failure" in {
      val player = system.actorOf(
        Player.props("test-actor-failures",
                     pingsPerPong = 1,
                     pongsPerSet = 10,
                     sets = 2,
                     pingDelay = 1 hour,
                     exceptionDelay = 1 hour)
      )
      player ! Ping
      expectMsg(Pong)
      player ! Ping
      expectMsg(Pong)
      player ! CurrentState
      val (state, stateData) = expectMsgClass(classOf[(PlayerState, PongStateData)])
      state === PongState
      stateData.receivedPings === 2
      player ! Oops
      player ! CurrentState
      val (_, stateDataAfterRecovering) = expectMsgClass(classOf[(PlayerState, PongStateData)])
      stateDataAfterRecovering.receivedPings === 2
      system.stop(player)
    }

    "game finish" in {
      val player = system.actorOf(
        Player.props("test-finish-game",
                     pingsPerPong = 1,
                     pongsPerSet = 1,
                     sets = 1,
                     pingDelay = 100 millis,
                     exceptionDelay = 1 hour))
      player ! StartGame(self)
      expectMsg(Ping)
      player ! Pong
      expectMsg(FinishGame)
    }

  }

  def createDefaultPlayerActor(name: String) = {
    TestActorRef(
      new Player(name, pingsPerPong = 4, pongsPerSet = 2, gamesSets = 5, pingDelay = 1 hour, errorDelay = 1 hour))
  }

  def defaultPlayerProps(name: String) =
    Player.props(name, pingsPerPong = 4, pongsPerSet = 2, sets = 5, pingDelay = 1 hour, exceptionDelay = 1 hour)

}
