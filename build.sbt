name := "ping-pong"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies += "com.typesafe.akka" %% "akka-actor"       % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit"     % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j"       % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-persistence" % "2.4.17"
libraryDependencies += "org.scalactic"     %% "scalactic"        % "3.0.1" % "test"
libraryDependencies += "org.scalatest"     %% "scalatest"        % "3.0.1" % "test"

mainClass in (Compile, run) := Some("com.game.player.PinPongGame")