### **Ping Pong Game**

----------

Implement Ping Pong game using Akka actors. There are two actors in the game. At the start of the game, the first actor sends a "ping" every **N** ms. The second actor responds with a "pong" message for every **M** sent "pings".  It is known that the actor who takes pings often fails with an error. It is necessary to simulate a breakdown every **B** ms > **N** and raise the broken actor with the value of the pings which was received. After **K** taken "pong" actors change places. The game of ping-pong is finished after **G** changes of the players.

----------

#### **Input parameters**

**N** - delay between pings
**B** - delay between breakdowns
**M** - the number of "pings" required for "pong"
**K** - the number of "pong", after which the actors change places
**G** - number of "sets", after which the game ends

----------

For running app in application dir use

```
sbt run
```

For running tests

```
sbt test
```